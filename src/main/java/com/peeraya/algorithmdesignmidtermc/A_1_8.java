/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.algorithmdesignmidtermc;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author ADMIN
 */
public class A_1_8 {

    public static void main(String[] args) {
//        int[] A = new int[]{3, 4, 1, 5};
        
        Scanner kb = new Scanner(System.in);
        int num = kb.nextInt();
        int [] A = new int[num];
        for (int i=0; i<num; i++) {
            A[i] = kb.nextInt();
        }
        
        reverse(A, A.length);
    }

    static void reverse(int[] A, int n) {

        for (int i = 0; i < n / 2; i++) {
            int x = A[i];
            A[i] = A[n - i - 1];
            A[n - i - 1] = x;
        }
        
        showReversedArrayInArrayForm(A);

//        showReversedArrayInNumericForm(n, A);
    }

    private static void showReversedArrayInArrayForm(int[] A) {
        // print reversed array is array form
        System.out.println("A = " + Arrays.toString(A));
    }

//    private static void showReversedArrayInNumericForm(int n, int[] A) {
//                
//        // print reversed array is numeric form
//        for (int k = 0; k < n; k++) {
//            System.out.print(A[k] + " ");
//        }
//        System.out.println();
//    }
}
